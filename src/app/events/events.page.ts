import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DatabaseService } from "../database.service";

@Component({
  selector: 'app-events',
  templateUrl: './events.page.html',
  styleUrls: ['./events.page.scss'],
})
export class EventsPage implements OnInit {
  events: any = [];

  constructor(private route:Router, public database: DatabaseService,) { }

  ngOnInit() {
    this.getEvents()
  }

  login() {
    this.route.navigate(['/login']);
  }
  registry() {
    this.route.navigate(['/registry']);
  }

  pastevents(){
    this.route.navigate(['/pastevents']);
  }

  getEvents() {
    this.database.getActiveEvents().then((data) => {
      this.events = [];
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          this.events.push(data.rows.item(i));
        }
      }
    }).catch(e=>console.log(JSON.stringify(e)));
  }

}
