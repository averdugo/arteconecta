import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registry',
  templateUrl: './registry.page.html',
  styleUrls: ['./registry.page.scss'],
})
export class RegistryPage implements OnInit {

  constructor(private route:Router) { }

  ngOnInit() {
  }

  login() {
    this.route.navigate(['/login']);
  }
  back() {
    this.route.navigate(['/events']);
  }
  pastevents(){
    this.route.navigate(['/pastevents']);
  }

}
