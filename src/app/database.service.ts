import { Injectable } from "@angular/core";
import { SQLite, SQLiteObject } from '@awesome-cordova-plugins/sqlite/ngx';


@Injectable({
  providedIn: "root",
})
export class DatabaseService {
  databaseObj!: SQLiteObject;
  events:any = [
    {img:"event1.png",lugar:"Estadio Nacional", fecha:"12 de abril de 2023", hora:"17:27", estado:0},
    {img:"event2.png",lugar:"Reserva Natural", fecha:"20 de abril de 2023", hora:"20:16", estado:0},
    {img:"event3.png",lugar:"Concepcion", fecha:"31 de mayo de 2022", hora:"21:00", estado:1},
    {img:"event4.png",lugar:"La Serena", fecha:"10 de febrero de 2021", hora:"21:00", estado:1},
  ]
  
  constructor(private sqlite: SQLite) {}

  createDatabase() {
    this.sqlite.create({
      name: 'arteconecta',
      location: 'default'
    })
      .then((db: SQLiteObject) => {
        this.databaseObj = db
        console.log("database creaaateeed ")
        this.createTables();
      })
      .catch(e => console.log(e));
  }

  async createTables() {
    await this.databaseObj.executeSql(
      `CREATE TABLE IF NOT EXISTS events (id INTEGER PRIMARY KEY AUTOINCREMENT, img VARCHAR(255) NOT NULL UNIQUE, lugar VARCHAR(255) NOT NULL, fecha VARCHAR(255) NOT NULL, hora VARCHAR(255) NOT NULL, estado INTEGER NOT NULL)`,
      []
    );

    this.events.forEach(async (e:any) => {
      await this.addEvents(e)
    });

    console.log("createTables creaaateeed ")
  }

  async addEvents(data:any) {
    return this.databaseObj
      .executeSql(
        `INSERT INTO events (img,lugar,fecha,hora, estado ) VALUES ('${data.img}', '${data.lugar}', '${data.fecha}', '${data.hora}', '${data.estado}')`,
        []
      )
      .then(() => {
        return "events created";
      })
      .catch((e) => {
        if (e.code === 6) {
          return "events already exists";
        }

        return "error on creating events " + JSON.stringify(e);
      });
  }

  async getActiveEvents() {
    return this.databaseObj.executeSql(
        `SELECT * FROM events where estado = 0`,
        []
      )
      .then((res) => {
        console.log("Data get", JSON.stringify(res))
        return res;
      })
      .catch((e) => {
        return "error on getting events " + JSON.stringify(e);
      });
  }

  async getinActiveEvents() {
    return this.databaseObj.executeSql(
        `SELECT * FROM events where estado = 1`,
        []
      )
      .then((res) => {
        console.log("Data get", JSON.stringify(res))
        return res;
      })
      .catch((e) => {
        return "error on getting events " + JSON.stringify(e);
      });
  }



  async editEvents(name: string, id: number) {
    return this.databaseObj
      .executeSql(
        `UPDATE events SET name = '${name}' WHERE id = ${id}`,
        []
      )
      .then(() => {
        return "events updated";
      })
      .catch((e) => {
        if (e.code === 6) {
          return "events already exist";
        }

        return "error on updating category " + JSON.stringify(e);
      });
  }

  
}