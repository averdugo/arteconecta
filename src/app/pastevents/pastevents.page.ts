import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DatabaseService } from '../database.service';

@Component({
  selector: 'app-pastevents',
  templateUrl: './pastevents.page.html',
  styleUrls: ['./pastevents.page.scss'],
})
export class PasteventsPage implements OnInit {
  events: any = [];
  constructor(private route:Router, public database:DatabaseService) { }

  ngOnInit() {
    this.getEvents()
  }

  login() {
    this.route.navigate(['/login']);
  }
  back() {
    this.route.navigate(['/events']);
  }

  getEvents() {
    this.database.getinActiveEvents().then((data) => {
      this.events = [];
      if (data.rows.length > 0) {
        for (var i = 0; i < data.rows.length; i++) {
          this.events.push(data.rows.item(i));
        }
      }
    }).catch(e=>console.log(JSON.stringify(e)));
  }

}
