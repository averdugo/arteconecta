import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PasteventsPage } from './pastevents.page';

const routes: Routes = [
  {
    path: '',
    component: PasteventsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PasteventsPageRoutingModule {}
