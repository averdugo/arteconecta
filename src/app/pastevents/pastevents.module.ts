import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PasteventsPageRoutingModule } from './pastevents-routing.module';

import { PasteventsPage } from './pastevents.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PasteventsPageRoutingModule
  ],
  declarations: [PasteventsPage]
})
export class PasteventsPageModule {}
